package com.example.annisamelyanta.annisa_1202164039_si4007_pab_modul2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    SimpleDateFormat dateFormatter;
    TextView tvTopUp, tvSaldo, tvTanggal, tvWaktu, tvTanggal2, tvWaktu2;
    EditText edSaldo, edTiket;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    Spinner spinner;
    Switch aSwitch;
    Button btnBeli;
    View dialogView;
    String saldo, pilihanTujuan;
    int hargaTotal;

    private  String[] listTujuan = {"Jakarta", "Cirebon", "Bekasi"};

   @Override
    protected void onCreate(Bundle savedInstanceState){
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_main);

       tvSaldo=(TextView)findViewById(R.id.tv_saldo);
       tvTopUp=(TextView)findViewById(R.id.tv_topUp);
       tvTopUp.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               tvSaldo.setText(null);
               DialogFrom();
           }
       });

       spinner=(Spinner)findViewById(R.id.sp_tujuan);
       ArrayAdapter<String> tujuanAdapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listTujuan);
       spinner.setAdapter(tujuanAdapter);
       spinner.setOnItemClickListener(this);

       dateFormatter=new SimpleDateFormat("dd-MM-yyyy", Locale.US);
       tvTanggal=(TextView)findViewById(R.id.tv_tanggal1);
       tvTanggal.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               showDateDialog();
           }
       });

       tvWaktu=(TextView)findViewById(R.id.tv_waktu1);
       tvWaktu.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               showTimeDialog();
           }
       });

       tvTanggal2=(TextView)findViewById(R.id.tv_tanggal2);
       tvTanggal2.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               showDateDialog2();
           }
       });

       tvWaktu2=(TextView)findViewById(R.id.tv_waktu2);
       tvWaktu2.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               showTimeDialog2();
           }
       });

       aSwitch=(Switch)findViewById(R.id.switch_pp);
       aSwitch.setChecked(false);
       aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if(isChecked){
                   tvTanggal2.setText("Pilih Tanggal");
                   tvWaktu2.setText("Pilih Waktu");
                   if(pilihanTujuan=="Jakarta"){
                       hargaTotal = 2 * 75000;
                   } else if (pilihanTujuan == "Cirebon"){
                       hargaTotal = 2 * 150000;
                   } else if (pilihanTujuan == "Bekasi") {
                       hargaTotal = 2 * 70000;
                   }
               } else {
                   tvTanggal2.setText("");
                   tvWaktu2.setText("");
               }
           }
       });

       if(aSwitch.isChecked()){
           tvTanggal2.setText("Pilih Tanggal");
           tvWaktu2.setText("Pilih Waktu");
       } else {
           tvTanggal2.setText("");
           tvWaktu2.setText("");
       }

       edTiket=(EditText)findViewById(R.id.ed_tiket);
       btnBeli=(Button)findViewById(R.id.btn_beli);
       btnBeli.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(MainActivity.this, CheckOut.class);

               intent.putExtra("tujuan", String.valueOf(spinner.getSelectedItem()));
               intent.putExtra("tanggal", tvTanggal.getText().toString());
               intent.putExtra("waktu", tvWaktu.getText().toString());
               intent.putExtra("tanggal2", tvTanggal2.getText().toString());
               intent.putExtra("waktu2", tvWaktu2.getText().toString());
               intent.putExtra("tiket", edTiket.getText().toString());
               intent.putExtra("hargaTotal", hargaTotal);
               startActivity(intent);
           }
       });

   }

    private void kosong(){
       edSaldo.setText(null);
    }

    private void showTimeDialog2() {
        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                tvWaktu2.setText(hourOfDay + ":" + minute);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

        timePickerDialog.show();
   }

    private void showDateDialog2() {
        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                tvTanggal2.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void showTimeDialog() {
        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                tvWaktu.setText(hourOfDay + ":" + minute);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

        timePickerDialog.show();
   }

    private void showDateDialog() {
       Calendar newCalendar = Calendar.getInstance();

       datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
           @Override
           public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
               Calendar newDate = Calendar.getInstance();
               newDate.set(year, month, dayOfMonth);
               tvTanggal.setText(dateFormatter.format(newDate.getTime()));
           }
       }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

       datePickerDialog.show();
    }

    private void DialogFrom() {
       dialog = new AlertDialog.Builder(MainActivity.this);
       inflater = getLayoutInflater();
       dialogView = inflater.inflate(R.layout.custom_alert_dialog, null);
       dialog.setView(dialogView);
       dialog.setCancelable(true);
       dialog.setTitle("Masukkan Jumlah Saldo");

       edSaldo = (EditText)dialogView.findViewById(R.id.tvSaldo);
       kosong();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        pilihanTujuan = listTujuan[position];
    }
}
